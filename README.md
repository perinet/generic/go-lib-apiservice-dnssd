# dnssd

This is the go implementation of the [api service
dns-sd](https://gitlab.com/perinet/unified-api/-/blob/main/services/DNS-SD_openapi.yaml)
of Perinets [unified API](https://gitlab.com/perinet/unified-api). This
implementation is tailored for linux based environments and targeted for lxc
based containers for the usage with Perinets Edge computer, the __periMICA__.

It implements an API to advertise as well as discover services with the DNS-SD
([RFC6763](https://www.ietf.org/rfc/rfc6763.txt)) protocol via mDNS
([RFC6762](http://www.ietf.org/rfc/rfc6762.txt)).


# Dual License

This software is by default licensed via the GNU Affero General Public License
version 3. However it is also available with a commercial license on request
(https://perinet.io/contact).