module gitlab.com/perinet/generic/apiservice/dnssd

go 1.22.0

require (
	github.com/alexandrevicenzi/go-sse v1.6.0
	github.com/godbus/dbus/v5 v5.1.0
	github.com/holoplot/go-avahi v1.0.2-0.20240210093433-b8dc0fc11e7e
	gitlab.com/perinet/generic/lib/httpserver v1.0.1
	gitlab.com/perinet/generic/lib/utils v1.0.1
	gitlab.com/perinet/periMICA-container/apiservice/node v1.0.7
	golang.org/x/exp v0.0.0-20241108190413-2d47ceb2692f
	gotest.tools/v3 v3.5.2-0.20240905034822-0b81523ff268
)

require (
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/handlers v1.5.2 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
)
