/**
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

// Go implementation of the [api service
// dns-sd](https://gitlab.com/perinet/unified-api/-/blob/main/services/DNS-SD_openapi.yaml)
// of Perinets [unified api](https://gitlab.com/perinet/unified-api).
package dnssd

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"gitlab.com/perinet/generic/apiservice/dnssd/dataModel"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"

	"github.com/alexandrevicenzi/go-sse"
	"gitlab.com/perinet/generic/apiservice/dnssd/internal/dnssdAdapter"
	"gitlab.com/perinet/generic/lib/httpserver"
)

const (
	API_VERSION = "24"
)

var serviceAdvertiser *dnssdAdapter.Advertiser
var serviceBrowser *dnssdAdapter.ServicesBrowser
var SSEServer *sse.Server

type DNSSDInfo = dataModel.DNSSDInfo
type DNSSDServiceInfo = dataModel.DNSSDServiceInfo
type DNSSDTxtRecordHttps = dataModel.DNSSDTxtRecordHttps

// implements internal ressource `/dns-sd/paths`, which declares routes and its access role
func PathsGet() []httpserver.PathInfo {
	return []httpserver.PathInfo{
		{Url: "/dns-sd", Method: httpserver.GET, Role: rbac.USER, Call: DNSSDInfoGet},

		{Url: "/dns-sd/advertised-services", Method: httpserver.GET, Role: rbac.ADMIN, Call: DNSSDServicesAdvertiseGet},
		{Url: "/dns-sd/advertised-services/{service_name}", Method: httpserver.GET, Role: rbac.ADMIN, Call: DNSSDServiceInfoAdvertiseGet},
		{Url: "/dns-sd/advertised-services/{service_name}", Method: httpserver.PATCH, Role: rbac.INTERNAL, Call: DNSSDServiceInfoAdvertiseSet},
		{Url: "/dns-sd/advertised-services/{service_name}", Method: httpserver.DELETE, Role: rbac.INTERNAL, Call: DNSSDServiceInfoAdvertiseRemove},
		{Url: "/dns-sd/discovered-services/", Method: httpserver.POST, Role: rbac.USER, Call: DNSSDServiceInfoDiscoverSet},
		{Url: "/dns-sd/discovered-services/", UrlIsPrefix: true, Method: httpserver.SSE, Role: rbac.USER},
		{Url: "/dns-sd/discovered-services/{service_name}", Method: httpserver.GET, Role: rbac.USER, Call: DNSSDServiceInfoDiscoverGet},
	}
}

func init() {
	go func() {
		for {
			tempServiceAdvertiser, err := dnssdAdapter.NewAdvertiser()
			if err != nil {
				log.Println("error while calling adapter.NewAdvertiser()", err)
				time.Sleep(2 * time.Second)
			} else {
				serviceAdvertiser = tempServiceAdvertiser
				break
			}
		}

		for {
			tempServiceBrowser, err := dnssdAdapter.NewServicesBrowser()
			if err != nil {
				log.Println("error while calling adapter.NewServicesBrowser()", err)
				time.Sleep(2 * time.Second)
			} else {
				serviceBrowser = tempServiceBrowser
				break
			}
		}
	}()
}

// implements getter for `/dns-sd/`, which provides the DNS-SD Info object
func DNSSDInfoGet(p periHttp.PeriHttp) {
	dnssdInfo := DNSSDInfo{ApiVersion: API_VERSION}

	if serviceAdvertiser != nil {
		dnssdInfo.Services = serviceAdvertiser.GetServices()
	}

	p.JsonResponse(http.StatusOK, dnssdInfo)
}

// implements getter for `/dns-sd/advertised-services`, which provides a list of
// all advertised dns-sd service names (e.g. _https._tcp)
func DNSSDServicesAdvertiseGet(p periHttp.PeriHttp) {
	if serviceAdvertiser == nil {
		p.JsonResponse(http.StatusOK, []string{})
	} else {
		p.JsonResponse(http.StatusOK, serviceAdvertiser.GetServices())
	}
}

// implements getter for `/dns-sd/advertised-services/{service_name}`, which
// provides an info object of a particular service name (e.g. _https._tcp)
func DNSSDServiceInfoAdvertiseGet(p periHttp.PeriHttp) {
	service_name, ok := p.Vars()["service_name"]
	if !ok {
		p.EmptyResponse(http.StatusBadRequest)
		return
	}

	if serviceAdvertiser == nil {
		log.Println("DNSSDServiceInfoAdvertiseGet error: serviceAdvertiser not yet initialized")
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "serviceAdvertiser not yet initialized"}`))
		return
	}

	serviceInfo := serviceAdvertiser.GetService(service_name)
	if serviceInfo == nil {
		p.EmptyResponse(http.StatusNotFound)
		return
	}
	p.JsonResponse(http.StatusOK, serviceInfo)
}

// implements setter for `/dns-sd/advertised-services/{service_name}`, which
// starts advertising a service, provided via the info object
func DNSSDServiceInfoAdvertiseSet(p periHttp.PeriHttp) {

	service_name, ok := p.Vars()["service_name"]
	if !ok {
		p.EmptyResponse(http.StatusBadRequest)
		return
	}

	payload, err := p.ReadBody()
	if err != nil {
		p.JsonResponse(http.StatusBadRequest, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	var serviceInfo DNSSDServiceInfo
	err = serviceInfo.UnmarshalJSON(payload)
	if err != nil {
		p.JsonResponse(http.StatusBadRequest, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	serviceInfo.ServiceName = service_name

	// set default values
	if serviceInfo.Port == 0 {
		serviceInfo.Port = 443
	}
	if serviceInfo.Weight == "" {
		serviceInfo.Weight = "0"
	}
	if serviceInfo.Priority == "" {
		serviceInfo.Priority = "0"
	}
	if serviceInfo.Ttl == "" {
		serviceInfo.Ttl = "300"
	}

	// wait for serviceAdvertiser init
	for range 20 {
		if serviceAdvertiser == nil {
			break
		}
		time.Sleep(10 * time.Millisecond)
	}

	if serviceAdvertiser == nil {
		log.Println("DNSSDServiceInfoAdvertiseSet error: serviceAdvertiser not yet initialized")
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "serviceAdvertiser not yet initialized"}`))
		return
	}

	err = serviceAdvertiser.AddService(serviceInfo)
	if err != nil {
		p.JsonResponse(http.StatusBadRequest, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.EmptyResponse(http.StatusNoContent)
}

// implements removal for `/dns-sd/advertised-services/{service_name}`, which
// stops advertising a service, provided via the info object
func DNSSDServiceInfoAdvertiseRemove(p periHttp.PeriHttp) {
	service_name, ok := p.Vars()["service_name"]
	if !ok {
		p.EmptyResponse(http.StatusBadRequest)
		return
	}

	if serviceAdvertiser == nil {
		log.Println("DNSSDServiceInfoAdvertiseRemove error: serviceAdvertiser not yet initialized")
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "serviceAdvertiser not yet initialized"}`))
		return
	}

	err := serviceAdvertiser.RemoveService(service_name)
	if err != nil {
		p.JsonResponse(http.StatusBadRequest, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.EmptyResponse(http.StatusNoContent)
}

// REMOVED in APIv24
// implements setter for `/dns-sd/discovered-services/{service_name}', which
// starts browsing service
func DNSSDServiceInfoDiscoverSet(p periHttp.PeriHttp) {
	p.EmptyResponse(http.StatusNotImplemented)
}

// implements getter for `/dns-sd/discovered-services/{service_name}`, which provides a list of
// all observed dns-sd service instances of a particular service type (e.g. _https._tcp) of the local domain
func DNSSDServiceInfoDiscoverGet(p periHttp.PeriHttp) {

	service_name, ok := p.Vars()["service_name"]
	if !ok {
		log.Println("No parameter! = ", service_name)
		p.EmptyResponse(http.StatusBadRequest)
		return
	}

	if serviceBrowser == nil {
		log.Println("DNSSDServiceInfoDiscoverGet error: serviceBrowser not yet initialized")
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "serviceBrowser not yet initialized"}`))
		return
	}

	p.Resp().Header().Set("Content-Type", "application/json")

	if !serviceBrowser.ServiceExists(service_name) {
		err := serviceBrowser.StartGetServiceInfos(service_name, send_sse_services)
		if err != nil {
			log.Println("DNSSDServiceInfoDiscoverGet error: ", err)
			p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
			return
		}
	}

	service_infos, err := serviceBrowser.GetServiceInfos(service_name)
	if err == dnssdAdapter.ErrNotRegistered {
		log.Println("DNSSDServiceInfoDiscoverGet error: service discover timed out or unknown")
		p.JsonResponse(http.StatusNotFound, json.RawMessage(`{"error": "service discover timed out or unknown"}`))
		return
	} else if err != nil {
		log.Println("DNSSDServiceInfoDiscoverGet error: ", err)
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	var service_bytes []byte
	service_bytes = append(service_bytes, '[')
	for i, service := range service_infos {
		jsonOut, _ := service.MarshalJSON()
		service_bytes = append(service_bytes, jsonOut...)
		if i < len(service_infos)-1 {
			service_bytes = append(service_bytes, ',')
		}
	}
	service_bytes = append(service_bytes, ']')

	if len(service_bytes) > 2 {
		p.JsonResponse(http.StatusOK, service_bytes)
	} else {
		p.EmptyResponse(http.StatusNoContent) // the search has started but no data
	}
}

func send_sse_services(service_name string, service dataModel.DNSSDEventMessage) {
	jsonMessage, err := json.Marshal(service)
	if err != nil {
		log.Println("send_sse_status marshal error: ", err)
		return
	}
	httpserver.SendSSE("/dns-sd/discovered-services/"+service_name, jsonMessage)
}
