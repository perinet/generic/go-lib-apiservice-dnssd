/**
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package dnssdAdapter

import (
	"encoding/json"
	"errors"
	"log"
	"net"
	"reflect"
	"strings"
	"sync"

	"github.com/godbus/dbus/v5"
	"github.com/holoplot/go-avahi"
	"gitlab.com/perinet/generic/apiservice/dnssd/dataModel"
)

var (
	ErrNotRegistered = errors.New("unknown service discover")
)

type ServicesBrowser struct {
	avahi_server *avahi.Server

	serviceTypes      []avahi.ServiceType
	serviceTypesMutex sync.RWMutex

	services      map[string][]avahi.Service
	servicesMutex sync.RWMutex

	logger log.Logger
}

// create a new ServicesBrowser
func NewServicesBrowser() (*ServicesBrowser, error) {
	sb := ServicesBrowser{}
	sb.services = map[string][]avahi.Service{}

	dbus_conn, err := dbus.SystemBus()
	if err != nil {
		log.Printf("Cannot get system bus: %v", err)
		return &sb, err
	}

	sb.avahi_server, err = avahi.ServerNew(dbus_conn)
	if err != nil {
		log.Printf("Avahi new failed: %v", err)
		return &sb, err
	}

	sb.logger = *log.Default()
	sb.logger.SetPrefix("dnssdAdapter.serviceBrowser:")

	return &sb, nil
}

func (sb *ServicesBrowser) StartBrowsingTypes() error {
	sb.serviceTypesMutex.Lock()
	if sb.serviceTypes != nil {
		sb.serviceTypes = []avahi.ServiceType{} // register service browser
		sb.serviceTypesMutex.Unlock()

		go sb.browseServiceTypes()
	} else {
		sb.serviceTypesMutex.Unlock()
		return errors.New("service type browsing already started")
	}

	return nil
}

func (sb *ServicesBrowser) browseServiceTypes() {
	if sb.avahi_server == nil {
		log.Println("error: no connection to avahi daemon")
		return
	}

	stb, err := sb.avahi_server.ServiceTypeBrowserNew(avahi.InterfaceUnspec, avahi.ProtoInet6, "local", 0)
	if err != nil {
		log.Printf("ServiceTypeBrowserNew() failed: %v", err)
		return
	}

	var serviceType avahi.ServiceType

	for {
		select {
		case serviceType = <-stb.AddChannel:
			sb.serviceTypesMutex.Lock()
			sb.serviceTypes = append(sb.serviceTypes, serviceType)
			sb.serviceTypesMutex.Unlock()
		case serviceType = <-stb.RemoveChannel:
			sb.serviceTypesMutex.Lock()
			for i, v := range sb.serviceTypes {
				if reflect.DeepEqual(v, serviceType) {
					sb.serviceTypes = append(sb.serviceTypes[:i], sb.serviceTypes[i+1:]...)
					break
				}
			}
			sb.serviceTypesMutex.Unlock()
		}
	}
}

func (sb *ServicesBrowser) GetBrowsedServiceTypes() ([]string, error) {
	if sb.serviceTypes == nil {
		return nil, ErrNotRegistered
	}

	var serviceTypes []string

	sb.serviceTypesMutex.RLock()
	for _, v := range sb.serviceTypes {
		serviceTypes = append(serviceTypes, v.Type)
	}
	sb.servicesMutex.RUnlock()

	return serviceTypes, nil
}

func (sb *ServicesBrowser) browseServices(service_name string, sseCallback func(service_name string, service dataModel.DNSSDEventMessage)) {
	if sb.avahi_server == nil {
		log.Println("error: no connection to avahi daemon")
		return
	}

	asb, err := sb.avahi_server.ServiceBrowserNew(avahi.InterfaceUnspec, avahi.ProtoInet6, service_name, "local", 0)
	if err != nil {
		log.Printf("ServiceBrowserNew() failed: %v", err)
		return
	}

	var service avahi.Service

	for {
		select {
		case service = <-asb.AddChannel:
			// if service type discovery meta-query
			if strings.HasPrefix(service_name, "_services._dns-sd._udp") {
				sb.servicesMutex.Lock()
				sb.services[service_name] = append(sb.services[service_name], service)
				sb.servicesMutex.Unlock()

				var message_to_sent dataModel.DNSSDEventMessage
				message_to_sent.ServiceInfo = dataModel.DNSSDServiceInfo{ServiceName: service.Type, Name: service.Name}

				message_to_sent.EventType = dataModel.SRV
				sseCallback(service_name, message_to_sent)
			} else {
				resolved_service, _ := sb.avahi_server.ResolveService(service.Interface, service.Protocol, service.Name,
					service.Type, service.Domain, avahi.ProtoInet6, 0)
				sb.servicesMutex.Lock()
				sb.services[service_name] = append(sb.services[service_name], resolved_service)
				sb.servicesMutex.Unlock()

				var addresses []net.IP
				ip := net.ParseIP(resolved_service.Address)
				if ip != nil {
					addresses = append(addresses, ip)
				}

				var obj dataModel.DNSSDTxtRecordHttps
				var txt_to_write []byte
				for _, slice := range resolved_service.Txt {
					txt_to_write = append(txt_to_write, slice...)
				}

				var message_to_sent dataModel.DNSSDEventMessage
				if err := json.Unmarshal(txt_to_write, &obj); err == nil {
					message_to_sent.ServiceInfo = dataModel.DNSSDServiceInfo{ServiceName: resolved_service.Type, Domain: resolved_service.Domain, Port: resolved_service.Port, Addresses: addresses, Hostname: resolved_service.Host, Name: resolved_service.Name, TxtRecord: obj}
				} else {
					var txt_records []string
					for _, v := range resolved_service.Txt {
						txt_records = append(txt_records, string(v))
					}
					message_to_sent.ServiceInfo = dataModel.DNSSDServiceInfo{ServiceName: resolved_service.Type, Domain: resolved_service.Domain, Port: resolved_service.Port, Addresses: addresses, Hostname: resolved_service.Host, Name: resolved_service.Name, TxtRecord: txt_records}
				}

				message_to_sent.EventType = dataModel.ADD
				sseCallback(service_name, message_to_sent)
			}
		case service = <-asb.RemoveChannel:
			sb.servicesMutex.Lock()
			for i, v := range sb.services[service_name] {
				if v.Name == service.Name {

					removed := sb.services[service_name][i]
					// send RMV event
					var message_to_sent dataModel.DNSSDEventMessage

					var addresses []net.IP
					ip := net.ParseIP(removed.Address)
					if ip != nil {
						addresses = append(addresses, ip)
					}

					var obj dataModel.DNSSDTxtRecordHttps
					var txt_to_write []byte
					for _, slice := range removed.Txt {
						txt_to_write = append(txt_to_write, slice...)
					}

					if err := json.Unmarshal(txt_to_write, &obj); err == nil {
						message_to_sent.ServiceInfo = dataModel.DNSSDServiceInfo{ServiceName: removed.Type, Domain: removed.Domain, Port: removed.Port, Addresses: addresses, Hostname: removed.Host, Name: removed.Name, TxtRecord: obj}

					} else {
						var txt_records []string
						for _, v := range removed.Txt {
							txt_records = append(txt_records, string(v))
						}
						message_to_sent.ServiceInfo = dataModel.DNSSDServiceInfo{ServiceName: removed.Type, Domain: removed.Domain, Port: removed.Port, Addresses: addresses, Hostname: removed.Host, Name: removed.Name, TxtRecord: txt_records}
					}

					message_to_sent.EventType = dataModel.RMV
					sseCallback(service_name, message_to_sent)

					sb.services[service_name] = append(sb.services[service_name][:i], sb.services[service_name][i+1:]...)
					break
				}
			}
			sb.servicesMutex.Unlock()
		}
	}
}

func (sb *ServicesBrowser) GetServiceInfos(service_name string) ([]dataModel.DNSSDServiceInfo, error) {
	matchingServices := []dataModel.DNSSDServiceInfo{}

	sb.servicesMutex.RLock()
	services, isRegistered := sb.services[service_name]
	sb.servicesMutex.RUnlock()

	if isRegistered {
		for _, service := range services {

			var addresses []net.IP
			ip := net.ParseIP(service.Address)
			if ip != nil {
				addresses = append(addresses, ip)
			}

			var obj dataModel.DNSSDTxtRecordHttps
			var txt_to_write []byte
			for _, slice := range service.Txt {
				txt_to_write = append(txt_to_write, slice...)
			}

			if err := json.Unmarshal(txt_to_write, &obj); err == nil {
				matchingServices = append(matchingServices, dataModel.DNSSDServiceInfo{ServiceName: service.Type, Domain: service.Domain, Port: service.Port, Addresses: addresses, Hostname: service.Host, Name: service.Name, TxtRecord: obj})

			} else {
				var txt_records []string
				for _, v := range service.Txt {
					txt_records = append(txt_records, string(v))
				}
				matchingServices = append(matchingServices, dataModel.DNSSDServiceInfo{ServiceName: service.Type, Domain: service.Domain, Port: service.Port, Addresses: addresses, Hostname: service.Host, Name: service.Name, TxtRecord: txt_records})
			}
		}
	} else {
		return matchingServices, ErrNotRegistered
	}

	return matchingServices, nil
}

func (sb *ServicesBrowser) StartGetServiceInfos(service_name string, sseCallback func(service_name string, service dataModel.DNSSDEventMessage)) error {
	if service_name == "" {
		return errors.New("empty service name")
	}

	sb.servicesMutex.Lock()
	sb.services[service_name] = []avahi.Service{} // register service browser
	sb.servicesMutex.Unlock()

	go sb.browseServices(service_name, sseCallback) // start service browser

	return nil
}

func (sb *ServicesBrowser) ServiceExists(serviceName string) bool {
	sb.servicesMutex.RLock()         // Acquire read lock
	defer sb.servicesMutex.RUnlock() // Ensure unlock after the function exits

	_, exists := sb.services[serviceName] // Check if the service exists in the map
	return exists                         // Return true if it exists, false otherwise
}
