/**
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package dnssdAdapter

import (
	"errors"
	"log"
	"sync"

	"github.com/godbus/dbus/v5"
	"github.com/holoplot/go-avahi"
	"gitlab.com/perinet/generic/apiservice/dnssd/dataModel"
)

type advertiseEntry struct {
	si dataModel.DNSSDServiceInfo
	eg *avahi.EntryGroup
}

type Advertiser struct {
	entries      map[string]advertiseEntry
	entriesMutex sync.RWMutex
	avahi_server *avahi.Server
	hostname     string
	fqdn         string
	logger       log.Logger
}

func NewAdvertiser() (*Advertiser, error) {
	a := Advertiser{}
	a.entries = map[string]advertiseEntry{}

	dbus_conn, err := dbus.SystemBus()
	if err != nil {
		log.Printf("Cannot get system bus: %v", err)
		return &a, err
	}

	a.avahi_server, err = avahi.ServerNew(dbus_conn)
	if err != nil {
		log.Printf("Avahi new failed: %v", err)
		return &a, err
	}

	a.hostname, err = a.avahi_server.GetHostName()
	if err != nil {
		log.Printf("GetHostName() failed: %v", err)
		return &a, err
	}

	a.fqdn, err = a.avahi_server.GetHostNameFqdn()
	if err != nil {
		log.Printf("GetHostNameFqdn() failed: %v", err)
		return &a, err
	}

	a.logger = *log.Default()
	a.logger.SetPrefix("dnssdAdapter.serviceAdvertiser:")

	return &a, nil
}

func (a *Advertiser) GetServices() []string {
	services := []string{}

	a.entriesMutex.RLock()
	for _, service := range a.entries {
		services = append(services, service.si.ServiceName)
	}
	a.entriesMutex.RUnlock()

	return services
}

func (a *Advertiser) GetService(service_name string) *dataModel.DNSSDServiceInfo {
	var advService advertiseEntry
	var exists bool

	// register a new service for advertisement or replace an advertised service

	a.entriesMutex.RLock()
	defer a.entriesMutex.RUnlock()

	if advService, exists = (a.entries)[service_name]; exists {
		log.Println("GetService: ", &advService.si)
		return &advService.si
	} else {
		log.Println("GetService: ", nil)
		return nil
	}
}

func (a *Advertiser) AddService(si dataModel.DNSSDServiceInfo) error {
	if a.avahi_server == nil {
		log.Println("error: no connection to avahi daemon")
		return errors.New("no connection to avahi daemon")
	}

	err := a.RemoveService(si.ServiceName)
	if err != nil {
		log.Printf("RemoveService failed: %v", err)
		return err
	}

	eg, err := a.avahi_server.EntryGroupNew()
	if err != nil {
		log.Printf("EntryGroupNew() failed: %v", err)
		return err
	}

	var avahi_txt [][]byte

	if txt, ok := si.TxtRecord.([]string); ok {
		for i := 0; i < len(txt); i++ {
			b := []byte(txt[i])
			avahi_txt = append(avahi_txt, b)
		}
	} else if dnssdTxtRecordHttps, ok := si.TxtRecord.(dataModel.DNSSDTxtRecordHttps); ok {
		txt := dnssdTxtRecordHttps.String()
		for i := 0; i < len(txt); i++ {
			b := []byte(txt[i])
			avahi_txt = append(avahi_txt, b)
		}
	} else {
		log.Println("error parsing DNS-SD TXT data")
		return errors.New("error parsing DNS-SD TXT data")
	}

	err = eg.AddService(avahi.InterfaceUnspec, avahi.ProtoUnspec, 0, a.hostname, si.ServiceName, "local", a.fqdn, si.Port, avahi_txt)
	if err != nil {
		log.Printf("AddService() failed: %v", err)
		return err
	}

	err = eg.Commit()
	if err != nil {
		log.Printf("Commit() failed: %v", err)
		return err
	}

	service := advertiseEntry{si, eg}
	a.entriesMutex.Lock()
	a.entries[si.ServiceName] = service
	a.entriesMutex.Unlock()

	return nil
}

func (a *Advertiser) RemoveService(service_name string) error {
	if a.avahi_server == nil {
		log.Println("error: no connection to avahi daemon")
		return errors.New("no connection to avahi daemon")
	}

	a.entriesMutex.Lock()
	defer a.entriesMutex.Unlock()

	if entry, exists := (a.entries)[service_name]; exists {
		err := entry.eg.Reset()
		if err != nil {
			log.Printf("Reset() failed: %v", err)
			return err
		}
	}

	return nil
}

// // convert object int byte array
// func convert_avahi_txt(record interface{}) [][]byte {
// 	var avahiTxt [][]byte
// 	v := reflect.ValueOf(record)
// 	t := reflect.TypeOf(record)

// 	for i := 0; i < v.NumField(); i++ {
// 		fieldValue := v.Field(i)
// 		fieldType := t.Field(i)
// 		jsonTag := fieldType.Tag.Get("json")

// 		if jsonTag == "" || jsonTag == "-" {
// 			continue
// 		}

// 		tagParts := strings.Split(jsonTag, ",")
// 		key := tagParts[0]

// 		var value string
// 		if fieldValue.Kind() == reflect.String {
// 			value = fieldValue.String()
// 		} else {
// 			value = fmt.Sprintf("%v", fieldValue.Interface())
// 		}

// 		avahiTxt = append(avahiTxt, []byte(key+"="+value))
// 	}
// 	return avahiTxt
// }
