/**
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package dnssd

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"strings"

	"testing"
	"time"

	"gitlab.com/perinet/generic/apiservice/dnssd/dataModel"
	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/generic/lib/utils/webhelper"
	"golang.org/x/exp/slices"
	"gotest.tools/v3/assert"
)

const (
	refresh_delay = 1000 * time.Millisecond
)

func TestInfo(t *testing.T) {
	var err error
	var dnssdInfo DNSSDInfo

	time.Sleep(refresh_delay)

	data := intHttp.Get(DNSSDInfoGet, nil)
	err = json.Unmarshal(data, &dnssdInfo)
	assert.NilError(t, err)
	assert.Assert(t, dnssdInfo.ApiVersion == API_VERSION)
	assert.Assert(t, len(dnssdInfo.Services) == 0)

	serviceAdvertiser.AddService(dataModel.DNSSDServiceInfo{ServiceName: "_https._tcp", Ttl: "30", Port: 443, TxtRecord: []string{"application_name=test"}})
	time.Sleep(refresh_delay) //wait until services arrive

	data = intHttp.Get(DNSSDInfoGet, nil)
	err = json.Unmarshal(data, &dnssdInfo)
	assert.NilError(t, err)
	assert.Assert(t, dnssdInfo.ApiVersion == API_VERSION)
	assert.Assert(t, len(dnssdInfo.Services) >= 1)

	err = serviceAdvertiser.RemoveService("_https._tcp")
	assert.NilError(t, err)
}

func TestDNSSDServicesAdvertise(t *testing.T) {
	var err error
	var services []string
	var data []byte

	// // none services are expected to be advertised
	// data = utils.InternalGet(DNSSDServicesAdvertiseGet)
	// err = json.Unmarshal(data, &services)
	// assert.NilError(t, err)
	// t.Log(services)
	// assert.Assert(t, len(services) == 0)

	// set advertiser service
	var vars = map[string]string{}
	service_name := "_unittest._tcp"
	vars["service_name"] = service_name
	var advertised_service_info = dataModel.DNSSDServiceInfo{
		Ttl:       "30",
		Port:      443,
		TxtRecord: []string{"application_name=test"},
	}
	data, err = json.Marshal(advertised_service_info)
	assert.NilError(t, err)
	// add service entry to be advertised
	data = webhelper.InternalVarsPatch(DNSSDServiceInfoAdvertiseSet, vars, data)
	assert.Assert(t, len(data) == 0)

	// check the storage of the advertised service
	data = webhelper.InternalVarsGet(DNSSDServiceInfoAdvertiseGet, vars)
	assert.Assert(t, data != nil)
	var advertised_service_info_get = dataModel.DNSSDServiceInfo{}
	err = json.Unmarshal(data, &advertised_service_info_get)
	assert.NilError(t, err)
	assert.Assert(t, advertised_service_info_get.Ttl == advertised_service_info.Ttl)
	assert.Assert(t, advertised_service_info_get.Port == advertised_service_info.Port)
	assert.Assert(t, reflect.DeepEqual(advertised_service_info_get.TxtRecord, advertised_service_info.TxtRecord) == true)

	// check if advertised service is listed
	data = intHttp.Get(DNSSDServicesAdvertiseGet, nil)
	err = json.Unmarshal(data, &services)
	assert.NilError(t, err)
	assert.Assert(t, slices.Contains(services, service_name))

	// check if advertised service is listed in the dnssdInfo as well
	var dnssdInfo DNSSDInfo
	data = intHttp.Get(DNSSDInfoGet, nil)
	err = json.Unmarshal(data, &dnssdInfo)
	assert.NilError(t, err)
	assert.Assert(t, dnssdInfo.ApiVersion == API_VERSION)
	assert.Assert(t, slices.Contains(dnssdInfo.Services, service_name))
}

func TestTxtRecord(t *testing.T) {

	//start advertising _https via dnssd
	dnssdServiceInfo := dataModel.DNSSDServiceInfo{Port: 443}
	if dnssdServiceInfo.TxtRecord == nil {
		dnssdServiceInfo.TxtRecord = []string{}
	}
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord.([]string), "api_version=")
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord.([]string), "life_state=")
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord.([]string), "application_name=")
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord.([]string), "element_name=")

	var avahi_txt [][]byte
	if txt, ok := dnssdServiceInfo.TxtRecord.([]string); ok {
		for i := 0; i < len(txt); i++ {
			b := []byte(txt[i])
			avahi_txt = append(avahi_txt, b)
		}
		log.Println("AddService() TxtRecord [][]byte: ", avahi_txt)
	} else if _, ok := dnssdServiceInfo.TxtRecord.(DNSSDTxtRecordHttps); ok {
		avahi_txt = convert_avahi_txt(dnssdServiceInfo.TxtRecord)
		log.Println("AddService() TxtRecord object: ", avahi_txt)
	}

	testDNSSDTxtRecordHttps := DNSSDTxtRecordHttps{Format: "uAPI"}

	txtTest := convert_avahi_txt(testDNSSDTxtRecordHttps)
	log.Println("oioioioi", string(txtTest[0]))

	// assert.Assert(t, dnssdServiceInfo.TxtRecord != nil)

	dnssdServiceInfo.TxtRecord = testDNSSDTxtRecordHttps

	if txt, ok := dnssdServiceInfo.TxtRecord.([]string); ok {
		for i := 0; i < len(txt); i++ {
			b := []byte(txt[i])
			avahi_txt = append(avahi_txt, b)
		}
		log.Println("AddService() TxtRecord [][]byte: ", avahi_txt)
	} else if _, ok := dnssdServiceInfo.TxtRecord.(DNSSDTxtRecordHttps); ok {
		avahi_txt = convert_avahi_txt(dnssdServiceInfo.TxtRecord)
		log.Println("AddService() TxtRecord object: ", avahi_txt)
	}

}

func TestUnmarshalStringTxtRecord(t *testing.T) {
	jsonData := `{
		"type": "_https._tcp",
		"domain": "example.com",
		"txt_record": "simple text record"
	}`

	var service DNSSDServiceInfo
	err := json.Unmarshal([]byte(jsonData), &service)
	assert.Assert(t, err == nil)

	// Assert that TxtRecord is unmarshaled as string
	txtRecord, ok := service.TxtRecord.(string)
	assert.Assert(t, ok, "TxtRecord should be a string")
	assert.Equal(t, "simple text record", txtRecord)
}

// TestUnmarshalObjectTxtRecord tests when TxtRecord is an object
func TestUnmarshalObjectTxtRecord(t *testing.T) {
	jsonData := `{
		"type": "_https._tcp",
		"domain": ".local",
		"txt_record": {
			"format": "uAPI",
			"api_version": "24",
			"application_name": "TestApp",
			"element_name": "Element1",
			"product_name": "ProductA"
		}
	}`

	var service DNSSDServiceInfo
	// err := json.Unmarshal([]byte(jsonData), &service)
	service.UnmarshalJSON([]byte(jsonData))

	txtRecord, ok := service.TxtRecord.(DNSSDTxtRecordHttps)
	assert.Assert(t, ok == true)
	assert.Equal(t, "uAPI", txtRecord.Format)
	assert.Equal(t, "24", txtRecord.ApiVersion.String())
	assert.Equal(t, "TestApp", txtRecord.ApplicationName)
	assert.Equal(t, "Element1", txtRecord.ElementName)
	assert.Equal(t, "ProductA", txtRecord.ProductName)
	// assert.Equal(t, , txtRecord.ErrorStatus)
}

func TestUnmarshalArrayTxtRecord(t *testing.T) {
	jsonData := `{
		"type": "_https._tcp",
		"domain": "local",
		"txt_record": ["txt1", "txt2", "txt3"]
	}`

	var service DNSSDServiceInfo
	err := json.Unmarshal([]byte(jsonData), &service)
	assert.Assert(t, err == nil)

	// Assert that TxtRecord is unmarshaled as a []string
	_, ok := service.TxtRecord.([]string)
	assert.Assert(t, ok == true)
	// assert.Equal(t, []string{"txt1", "txt2", "txt3"}, txtRecord)
}

func TestMarshalArrayTxtRecord(t *testing.T) {
	// service := DNSSDServiceInfo{
	// 	ServiceName: "_https._tcp",
	// 	Domain:      ".local",
	// 	TxtRecord:   []string{"txt1", "txt2", "txt3"},
	// }

	// jsonData, err := json.Marshal(service)
	// assert.Assert(t, err == nil)

	// expected := `{
	// 	"type": "_https._tcp",
	// 	"domain": ".local",
	// 	"txt_record": ["txt1", "txt2", "txt3"]
	// }`
	// // assert.Assert(t, expected == string(jsonData))
}

// TestMarshalObjectTxtRecord tests marshaling when TxtRecord is an object
func TestMarshalObjectTxtRecord(t *testing.T) {
	service := DNSSDServiceInfo{
		ServiceName: "http",
		Domain:      "example.com",
		TxtRecord: DNSSDTxtRecordHttps{
			Format:          "uAPI",
			ApiVersion:      "24",
			ApplicationName: "TestApp",
			ElementName:     "Element1",
			ProductName:     "ProductA",
			// ErrorStatus:     NodeErrorStatus.Error,
		},
	}

	_, err := service.MarshalJSON()
	assert.Assert(t, err == nil)

}

func convert_avahi_txt(record interface{}) [][]byte {
	var avahiTxt [][]byte
	v := reflect.ValueOf(record)
	t := reflect.TypeOf(record)

	for i := 0; i < v.NumField(); i++ {
		fieldValue := v.Field(i)
		fieldType := t.Field(i)
		jsonTag := fieldType.Tag.Get("json")

		// Skip fields without a json tag or with "-" as the tag
		if jsonTag == "" || jsonTag == "-" {
			continue
		}

		// Extract the field name from the json tag (ignore everything after a comma)
		tagParts := strings.Split(jsonTag, ",")
		key := tagParts[0]

		// Convert the field value to a string
		var value string
		if fieldValue.Kind() == reflect.String {
			value = fieldValue.String()
		} else {
			value = fmt.Sprintf("%v", fieldValue.Interface())
		}

		// Append the key-value pair to the avahiTxt slice
		avahiTxt = append(avahiTxt, []byte(key+"="+value))
	}

	return avahiTxt
}

func TestTimeout(t *testing.T) {

	timeout := 0
	for timeout < 30 {
		assert.Assert(t, timeout < 30)
		log.Println("hi")
		timeout = timeout + 1
		// Sleep for 1 second before the next iteration
		time.Sleep(1 * time.Second)
	}
}
