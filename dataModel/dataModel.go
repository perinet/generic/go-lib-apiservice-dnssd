/**
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package dataModel

import (
	"encoding/json"
	"fmt"
	"log"
	"net"

	"gitlab.com/perinet/periMICA-container/apiservice/node"
)

type DNSSDInfo struct {
	ApiVersion  json.Number          `json:"api_version"`
	Services    []string             `json:"services"`
	ErrorStatus node.NodeErrorStatus `json:"error_status"`
}

// type DNSSDTxtRecord = map[string]string
type DNSSDTxtRecord = []string

type DNSSDTxtRecordHttps struct {
	Format          string               `json:"format,omitempty"`
	ApiVersion      json.Number          `json:"api_version,omitempty"`
	ApplicationName string               `json:"application_name,omitempty"`
	ElementName     string               `json:"element_name,omitempty"`
	ProductName     string               `json:"product_name,omitempty"`
	ErrorStatus     node.NodeErrorStatus `json:"error_status,omitempty"`
}

func (r DNSSDTxtRecordHttps) String() DNSSDTxtRecord {
	var dnssdTxtRecord DNSSDTxtRecord

	dnssdTxtRecord = append(dnssdTxtRecord, "format="+r.Format)
	dnssdTxtRecord = append(dnssdTxtRecord, "api_version="+r.ApiVersion.String())
	dnssdTxtRecord = append(dnssdTxtRecord, "application_name="+r.ApplicationName)
	dnssdTxtRecord = append(dnssdTxtRecord, "element_name="+r.ElementName)
	dnssdTxtRecord = append(dnssdTxtRecord, "product_name="+r.ProductName)
	dnssdTxtRecord = append(dnssdTxtRecord, "error_status="+r.ErrorStatus.String())

	return dnssdTxtRecord
}

// DnssdServiceInfo - https://www.rfc-editor.org/rfc/rfc2782
type DNSSDServiceInfo struct {

	// DNS label pair according to rfc6763.
	ServiceName string `json:"type,omitempty"`

	Domain string `json:"domain,omitempty"`

	Priority json.Number `json:"priority,omitempty"`

	Weight json.Number `json:"weight,omitempty"`

	Ttl json.Number `json:"ttl,omitempty"`

	Port uint16 `json:"port,omitempty"`

	Hostname string `json:"hostname,omitempty"`

	Name string `json:"name,omitempty"`

	Addresses []net.IP `json:"addresses,omitempty"`

	TxtRecord any `json:"txt_record,omitempty"`
}

type EventType string

const (
	ADD EventType = "ADD"
	RMV EventType = "RMV"
	SRV EventType = "SRV"
)

type DNSSDEventMessage struct {
	ServiceInfo DNSSDServiceInfo `json:"service_info"`
	EventType   EventType        `json:"event_type"`
}

func (m *DNSSDServiceInfo) UnmarshalJSON(data []byte) error {
	// Use a temporary structure to hold the TxtRecord as raw JSON data.
	var raw struct {
		ServiceName string          `json:"type,omitempty"`
		Domain      string          `json:"domain,omitempty"`
		Priority    json.Number     `json:"priority,omitempty"`
		Weight      json.Number     `json:"weight,omitempty"`
		Ttl         json.Number     `json:"ttl,omitempty"`
		Port        uint16          `json:"port,omitempty"`
		Hostname    string          `json:"hostname,omitempty"`
		Name        string          `json:"name,omitempty"`
		Addresses   []net.IP        `json:"addresses,omitempty"`
		TxtRecord   json.RawMessage `json:"txt_record"`
	}

	if err := json.Unmarshal(data, &raw); err != nil {
		return fmt.Errorf("error unmarshalling raw struct: %w", err)
	}

	m.ServiceName = raw.ServiceName
	m.Domain = raw.Domain
	m.Priority = raw.Priority
	m.Weight = raw.Weight
	m.Ttl = raw.Ttl
	m.Port = raw.Port
	m.Hostname = raw.Hostname
	m.Name = raw.Name
	m.Addresses = raw.Addresses

	var obj DNSSDTxtRecordHttps
	if err := json.Unmarshal(raw.TxtRecord, &obj); err == nil {
		m.TxtRecord = obj
		return nil
	} else {
		log.Printf("Failed to unmarshal TxtRecord as object: %v\n", err)
	}

	var strArray []string
	if err := json.Unmarshal(raw.TxtRecord, &strArray); err == nil {
		m.TxtRecord = strArray
		return nil
	} else {
		log.Printf("Failed to unmarshal TxtRecord as array: %v\n", err)
	}

	var str string
	if err := json.Unmarshal(raw.TxtRecord, &str); err == nil {
		m.TxtRecord = str
		return nil
	} else {
		log.Printf("Failed to unmarshal TxtRecord as string: %v\n", err)
	}

	return fmt.Errorf("could not unmarshal TxtRecord as object, array, or string")
}

func (m DNSSDServiceInfo) MarshalJSON() ([]byte, error) {
	type Alias DNSSDServiceInfo
	return json.Marshal(&struct {
		TxtRecord any `json:"txt_record"`
		Alias
	}{
		TxtRecord: m.TxtRecord, // This will be either string, []string, or DNSSDTxtRecordHttps
		Alias:     (Alias)(m),
	})
}

func (e DNSSDEventMessage) MarshalJSON() ([]byte, error) {
	type Alias DNSSDEventMessage
	return json.Marshal(&struct {
		ServiceInfo json.RawMessage `json:"service_info"`
		EventType   EventType       `json:"event_type"`
	}{

		ServiceInfo: func() json.RawMessage {
			serviceInfoBytes, _ := e.ServiceInfo.MarshalJSON()
			return serviceInfoBytes
		}(),
		EventType: e.EventType,
	})
}
